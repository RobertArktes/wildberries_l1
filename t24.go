package main

import (
	"fmt"
	"math"
)

type Point struct {
	x float64
	y float64
}

//Конструктор
func NewPoint(x float64, y float64) *Point {
	return &Point{x, y}
}

func main() {
	//Инициализация двух точек
	p1 := NewPoint(1, 1)
	p2 := NewPoint(7, 9)

	//Поиск расстояние через теорему Пифагора
	x := p1.x - p2.x
	y := p1.y - p2.y
	z := math.Sqrt(x*x + y*y)
	fmt.Println("Расстояние -", z)
}

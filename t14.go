package main

import (
	"fmt"
	"reflect"
)

func main() {
	//Объявление разных типов
	var a int
	var b string
	var c bool
	var d chan string

	spoter(a)
	spoter(b)
	spoter(c)
	spoter(d)
}

func spoter(val interface{}) {
	//Определение типов
	fmt.Println(reflect.TypeOf(val))
}

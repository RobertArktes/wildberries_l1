package main

import (
	"fmt"
	"strings"
)

var justString string

func createHugeString() string {
	return strings.Repeat("example_huge_string_", 500)
}

func someFunc() {
	v := createHugeString()
	arr := strings.Split(v, "")
	arr = arr[:100]
	v = strings.Join(arr, "")
	fmt.Println(v)
}

func main() {
	someFunc()
}

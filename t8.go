package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	var num int64
	var i int

	fmt.Println("Введите число")
	fmt.Scanln(&num)
	fmt.Println("Введите номер бита")
	fmt.Scanln(&i)

	str := fmt.Sprintf("%064b\n", num) //Перевод в двоичный код
	splt := strings.Split(str, "")

	//Замена бита
	if splt[i] == "1" {
		splt[i] = "0"
	} else {
		splt[i] = "1"
	}

	str = strings.Join(splt, "")
	str = strings.TrimSuffix(str, "\n")    //Удаление "\n"
	n, err := strconv.ParseInt(str, 2, 64) //Перевод обратно в int
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(n)
}

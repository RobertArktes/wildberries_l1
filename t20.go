package main

import (
	"fmt"
	"strings"
)

func Reverse(s []string) []string {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return s
}

func main() {
	str := "snow dog sun"

	s := strings.Split(str, " ")
	Reverse(s)

	fmt.Println(strings.Join(s, " "))
}

package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
	num := 5

	fmt.Println(binSearch(num, arr))
}

func binSearch(num int, arr []int) int {
	l := len(arr)
	l = l / 2
	if arr[l] == num {
		return arr[l]
	} else if arr[l] > num {
		a := arr[:l]
		n := binSearch(num, a)

		return n
	} else if arr[l] < num {
		a := arr[l:]
		n := binSearch(num, a)

		return n
	}

	return 0
}

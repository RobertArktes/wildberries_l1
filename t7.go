package main

import (
	"fmt"
	"sync"
	"time"
)

var m = map[int]int{}

func main() {
	var lock = sync.RWMutex{} //Создание мьютекса
	c := make(chan int)

	go writer(c)
	//Создание пула воркеров
	for i := 0; i < 3; i++ {
		go reader(c, &lock)
	}

	time.Sleep(1 * time.Millisecond)
	lock.Lock()
	fmt.Println(m)
	lock.Unlock()
}

func writer(c chan int) {
	i := 0
	for {
		c <- i
		i++
	}
}

func reader(c chan int, lock *sync.RWMutex) {
	for {
		i := <-c
		lock.Lock() //Лок для записи
		m[i] = i
		lock.Unlock() //Анлок
	}
}

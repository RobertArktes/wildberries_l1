package main

import (
	"fmt"
	"strconv"
)

func main() {
	arr := []float64{-25.4, -27.0, 13.0, 19.0, 15.5, 24.5, -21.0, 32.5}
	var m = make(map[int][]float64)
	var negative []float64
	var positive []float64

	for i := range arr {
		if arr[i] < 0 {
			negative = append(negative, arr[i])
		} else {
			positive = append(positive, arr[i])
		}
	}

	for i := range negative {
		t := fmt.Sprint(negative[i])
		temp := []byte(t)
		key, _ := strconv.Atoi(string(temp[1]))
		nums := append(m[-key], negative[i])
		m[-key] = nums
	}

	for i := range positive {
		t := fmt.Sprint(positive[i])
		temp := []byte(t)
		key, _ := strconv.Atoi(string(temp[0]))
		nums := append(m[key], positive[i])
		m[key] = nums
	}

	fmt.Println(m)
}

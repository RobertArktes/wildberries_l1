package main

import (
	"fmt"
	"time"
)

func main() {
	var num int
	fmt.Print("Введите количество секунд - ")
	fmt.Scanln(&num)

	c := make(chan int)
	go reader(c)
	go writer(c)

	time.Sleep(time.Duration(num) * time.Second)

	fmt.Println("Программа отработала")
}

func writer(c chan int) {
	i := 0
	for {
		c <- i
		i++
	}
}

func reader(c chan int) {
	for {
		fmt.Println(<-c)
	}
}

package main

import (
	"fmt"
	"time"
)

func main() {
	arr := [5]int{2, 4, 6, 8, 10}

	for _, n := range arr {
		go fmt.Println(n * n) //Расчет квадрата и вывод его в отдельной горутине
	}

	time.Sleep(5 * time.Millisecond) //Ожидание в 5 миллисекунд, чтобы все горутины выполнились
}

//Вывод ответов будет в случайном порядке

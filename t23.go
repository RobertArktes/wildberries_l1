package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
	del := 5

	copy(arr[del:], arr[del+1:])
	arr[len(arr)-1] = 0
	arr = arr[:len(arr)-1]

	fmt.Println(arr)
}

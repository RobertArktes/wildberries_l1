package main

import "fmt"

func Reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func main() {
	fmt.Println(Reverse("1 2 3 4 5"))
	fmt.Println(Reverse("А роза упала на лапу Азора"))
	fmt.Println(Reverse("Карл украл у Клары коралы"))
}

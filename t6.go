package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	cancelWithCloseV1()
	cancelWithBool()
	cancelWithContext()
}

func cancelWithCloseV1() {
	quit := make(chan struct{})

	go func() {
		for {
			select {
			case <-quit:
				return
			default:
				fmt.Println("Bonjour le monde!")
			}
		}
	}()

	time.Sleep(1 * time.Microsecond)
	close(quit)
}

func cancelWithBool() {
	die := make(chan bool)

	go func() {
		for {
			select {
			// ... выполняем что-нибудь в других case
			case <-die:
				// ... выполняем необходимые действия перед завершением.
				die <- true
				fmt.Println("Not clear! Not clear!")
				return
			default:
				fmt.Println("Sector is clear")
			}
		}
	}()

	time.Sleep(1 * time.Microsecond)
	die <- true

	// Ждем, пока все горутины закончат выполняться
	<-die
	time.Sleep(1 * time.Microsecond)
}

func cancelWithContext() {
	gen := func(ctx context.Context) <-chan int {
		dst := make(chan int)
		n := 1
		go func() {
			for {
				select {
				case <-ctx.Done():
					return // returning not to leak the goroutine
				case dst <- n:
					n++
				}
			}
		}()
		return dst
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // cancel when we are finished consuming integers

	for n := range gen(ctx) {
		fmt.Println(n)
		if n == 5 {
			break
		}
	}
}

package main

import (
	"fmt"
	"math/big"
)

func main() {
	a := new(big.Float)
	a.SetString("2000000")
	b := new(big.Float)
	b.SetString("1000001")

	answer := new(big.Float)

	fmt.Println("Сложение - ", answer.Add(a, b))
	fmt.Println("Умножение - ", answer.Mul(a, b))
	fmt.Println("Вычитание - ", answer.Sub(a, b))
	fmt.Println("Деление - ", answer.Quo(a, b))
}

package main

import "fmt"

type Human struct {
	Name string
	Age  int
}

func (h *Human) Sing() {
	fmt.Println("I'm only human after all")
}

type Action struct {
	Human
}

func main() {
	hum := Human{"Black Beard", 40} //Создние экземпляр структуры Human
	act := Action{hum}              //Передача hum в экземпляр структуры Action

	act.Human.Sing() //Реализация метода Sing
}

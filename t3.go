package main

import (
	"fmt"
	"time"
)

func main() {
	arr := [5]int{2, 4, 6, 8, 10}
	var sum int

	for _, n := range arr {
		go square(&sum, n)
	}

	time.Sleep(10 * time.Millisecond)

	fmt.Println(sum)
}

//Функция принимает значение n, возводит его в квадрат и записывает напрямую в сумму
func square(sum *int, n int) {
	*sum += n * n
}
